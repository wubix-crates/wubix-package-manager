# `wpm`, Wubix Package Manager

Every mature operating system has a package manager for installing programs, and Wubix is no
exception. `wpm` lets you easily discover, install and manage Wubix programs.

## How to use

`wpm` should be already available in your Wubix installation, so you don't need to install it.
To see all packages avaiable to install, run `wpm list`:

```
$ wpm list
Fetching packages... done
- tinysh
- coreutils installed v0.0.1
- upload-file
- wpm installed v0.1.1
- lolcat
```

To install a package, run `wpm install`:

```
$ wpm install lolcat
Going to install:
- lolcat v1.2.0
  - /usr/bin/lolcat
Proceed to install? (yes/no): y
Installing lolcat...
- Installed lolcat
Finished installation.
```

To see information about a package, run `wpm info`:

```
$ wpm info lolcat
       Name: lolcat
Description: The good ol' lolcat, now with fearless concurrency.
    Version: 1.2.0 installed v1.2.0
Executables: - /usr/bin/lolcat
```

To update a package, run `wpm update`:

```
$ wpm update coreutils
Going to update:
- coreutils from v0.0.1 to v0.0.2
  - /bin/pwd will be updated
  - /bin/cat will be updated
  # ...
  - /bin/rmdir will be installed
Process to update? (yes/no): y
Updating coreutils...
- Updated pwd
- Updated cat
# ...
- Installed rmdir
Finished update.
```

To remove a package, run `wpm remove`:

```
$ wpm remove wpm
Going to remove:
- wpm v0.1.1
  - /usr/bin/wpm
Proceed to remove? (yes/no): y
Removing wpm. Good luck!
- Removed wpm
Finished removal.
```

## Wubix Package Registry

`wpm` uses the [Wubix Package Registry]. If you want to edit the registry or just learn about it
more, head to its repository.

[Wubix Package Registry]: https://gitlab.com/wubix-crates/wubix-package-registry
