use crate::{arguments::List, local_state::LocalState, registry::fetch_registry, Result};
use librust::{print, println};
use reqwest::Client;

pub async fn list(client: &Client, options: List) -> Result<()> {
    let local_state = LocalState::open().await?;
    let packages = fetch_registry(client).await?.packages;

    let has_packages_to_show = if let Some(search) = &options.search {
        let search = search.to_lowercase();
        packages
            .iter()
            .any(|package| package.to_lowercase().contains(&search))
    } else {
        !packages.is_empty()
    };

    if !has_packages_to_show {
        bunt::eprintln!("{$red}No package matched the query{/$}").await;
        librust::process::exit(1);
    };

    for package_name in packages {
        if let Some(search) = &options.search {
            let search = search.to_lowercase();

            let package_name_lowercased = package_name.to_lowercase();
            if !package_name_lowercased.contains(&search) {
                continue;
            }

            bunt::print!("{$dimmed}-{/$} ").await;

            let mut char_indices = package_name.char_indices();
            while let Some((index, character)) = char_indices.next() {
                if package_name_lowercased[index..].starts_with(&search) {
                    bunt::print!("{$green}{}{/$}", &package_name[index..index + search.len()])
                        .await;

                    (1..search.chars().count()).for_each(|_| {
                        let _ = char_indices.next();
                    });
                } else {
                    print!("{}", character).await;
                }
            }
        } else {
            bunt::print!("{$dimmed}-{/$} {}", package_name).await;
        }

        match local_state.installed_packages.get(&package_name) {
            Some(local_package) => {
                bunt::println!(" {$dimmed}installed v{}{/$}", local_package.version).await;
            }
            None => println!().await,
        }
    }

    Ok(())
}
