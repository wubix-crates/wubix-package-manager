use crate::{
    arguments::Remove,
    local_state::LocalState,
    utilities::{confirm, ShowFiles},
    Result,
};
use librust::{
    eprintln,
    fs::remove_file,
    io::{stderr, Write},
};
use std::collections::HashMap;

pub async fn remove(options: Remove) -> Result<()> {
    if options.packages.is_empty() {
        eprintln!("Nothing to remove.").await;
        return Ok(());
    }

    let mut local_state = LocalState::open().await?;
    let mut packages = HashMap::new();

    let mut met_unknown_packages = false;
    for package_name in options.packages {
        if let Some(package) = local_state.installed_packages.get(&package_name) {
            packages.insert(package_name, package.clone());
        } else {
            bunt::eprintln!(
                "{$red}Error:{/$} no package named {$green}{}{/$} is installed",
                package_name,
            )
            .await;
            met_unknown_packages = true;
        }
    }

    if met_unknown_packages {
        librust::process::exit(1);
    }

    eprintln!("Going to remove:").await;
    for (package_name, package) in &packages {
        bunt::println!(
            "{$dimmed}-{/$} {$bold}{}{/$} {$dimmed}v{}{/$}",
            package_name,
            package.version,
        )
        .await;
        ShowFiles::stderr()
            .padding(2)
            .show(package.files.keys().map(String::as_str))
            .await?;
    }

    if !confirm("Proceed to remove?", options.assume).await? {
        eprintln!("Stopping removal.").await;
        return Ok(());
    }

    for (package_name, package) in packages {
        if package_name == "wpm" {
            bunt::eprintln!("Removing {$green+bold}wpm{/$}. Good luck!").await;
        } else {
            bunt::eprintln!("Removing {$green+bold}{}{/$}...", package_name).await;
        }

        for file_path in package.files.keys() {
            bunt::eprint!("{$dimmed}-{/$} Removing {$green}{}{/$}...\r", file_path).await;
            stderr().flush().await?;
            remove_file(&file_path).await?;
            bunt::eprintln!("{$dimmed}-{/$} Removed {$green}{}{/$}    ", file_path).await;
        }

        local_state.installed_packages.remove(&package_name);
        local_state.write().await?;
    }

    eprintln!("Finished removal.").await;
    Ok(())
}
