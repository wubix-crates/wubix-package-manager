use crate::{
    arguments::Update,
    local_state::{InstalledPackage, LocalState},
    registry::{download_file, fetch_package},
    utilities::{check_for_conflicts, confirm, ShowFiles},
    Result,
};
use librust::{
    eprintln,
    fs::{remove_file, File, Permissions},
    io::{stderr, Write},
};
use reqwest::Client;
use semver::Version;
use std::collections::{HashMap, HashSet};

pub async fn update(client: &Client, mut options: Update) -> Result<()> {
    let mut local_state = LocalState::open().await?;
    let mut packages = HashMap::new();

    if options.packages.is_empty() {
        options.packages = local_state.installed_packages.keys().cloned().collect();
    }

    let mut met_unknown_packages = false;
    for package_name in options.packages {
        let installed_package = match local_state.installed_packages.get(&package_name) {
            Some(package) => package,
            None => {
                bunt::eprintln!(
                    "{$red}Error:{/$} no package named {$green}{}{/$} is installed",
                    package_name,
                )
                .await;
                met_unknown_packages = true;
                continue;
            }
        };

        let package = if let Some(package) = fetch_package(client, &package_name).await? {
            package
        } else {
            bunt::eprintln!(
                "{$yellow}Warning:{/$} {$green}{}{/$} is installed, it wasn't found in the remote \
                registry. Skipping it.",
                package_name,
            )
            .await;
            continue;
        };

        let remote_version = Version::parse(&package.version)?;
        let installed_version = Version::parse(&installed_package.version)?;

        if remote_version == installed_version {
            bunt::eprintln!("Package {$green}{}{/$} is up to date.", package_name,).await;
            continue;
        }

        packages.insert(package_name, package);
    }

    if met_unknown_packages {
        librust::process::exit(1);
    }

    if packages.is_empty() {
        eprintln!("Nothing to update.").await;
        return Ok(());
    }

    if check_for_conflicts(&local_state, &packages).await? {
        eprintln!("Stopping update due to conflicts.").await;
        librust::process::exit(1);
    }

    eprintln!("Going to update:").await;
    for (package_name, package) in &packages {
        let installed = &local_state.installed_packages[package_name];

        bunt::println!(
            "{$dimmed}-{/$} {$bold}{}{/$} {$dimmed}from v{} to v{}{/$}",
            package_name,
            installed.version,
            package.version,
        )
        .await;

        let remote_files: HashSet<_> = package.file_paths().collect();
        let all_files: HashSet<_> = remote_files
            .iter()
            .cloned()
            .chain(installed.files.keys().map(String::as_str))
            .collect();
        ShowFiles::stderr()
            .padding(2)
            .show_with(all_files.into_iter(), |file| {
                let is_remote = remote_files.contains(file);
                let is_local = installed.files.contains_key(file);
                let needs_to_update =
                    package.files.get(file).map(|x| &x.fingerprint) == installed.files.get(file);

                async move {
                    match (is_remote, is_local) {
                        (true, true) if needs_to_update => {
                            bunt::eprint!(" {$dimmed}doesn't need to update{/$}").await
                        }
                        (true, true) => bunt::eprint!(" {$dimmed}will be updated{/$}").await,
                        (true, false) => bunt::eprint!(" {$dimmed}will be installed{/$}").await,
                        (false, true) => bunt::eprint!(" {$red+bold}will be removed{/$}").await,
                        (false, false) => (), // unreachable
                    }
                }
            })
            .await?;
    }

    if !confirm("Proceed to update?", options.assume).await? {
        eprintln!("Stopping update.").await;
        return Ok(());
    }

    for (package_name, package) in packages {
        bunt::eprintln!("Updating {$green+bold}{}{/$}...", package_name).await;

        let updating_package = InstalledPackage {
            files: package
                .files
                .iter()
                .map(|(path, info)| (path.clone(), info.fingerprint.clone()))
                .collect(),
            version: package.version,
        };

        let installed_package = &local_state.installed_packages[&package_name];
        let all_files: HashSet<_> = updating_package
            .files
            .keys()
            .chain(installed_package.files.keys())
            .map(String::as_str)
            .collect();

        for file_path in all_files {
            let is_local = installed_package.files.contains_key(file_path);

            if package.files.get(file_path).map(|x| &x.fingerprint)
                == installed_package.files.get(file_path)
            {
                continue;
            }

            if let Some(info) = package.files.get(file_path) {
                bunt::eprint!("{$dimmed}-{/$} Downloading {$green}{}{/$}...\r", file_path).await;
                stderr().flush().await?;
                let file_contents = download_file(client, &package_name, &info.fingerprint).await?;

                if is_local {
                    bunt::eprint!("{$dimmed}-{/$} Updating {$green}{}{/$}...   \r", file_path)
                        .await;
                } else {
                    bunt::eprint!("{$dimmed}-{/$} Installing {$green}{}{/$}... \r", file_path)
                        .await;
                }

                stderr().flush().await?;
                let mut file = File::create(file_path).await?;
                file.write_all(&file_contents).await?;
                file.set_permissions(Permissions::from_mode(info.permissions))
                    .await?;

                if is_local {
                    bunt::eprintln!("{$dimmed}-{/$} Updated {$green}{}{/$}       ", file_path)
                        .await;
                } else {
                    bunt::eprintln!("{$dimmed}-{/$} Installed {$green}{}{/$}     ", file_path)
                        .await;
                }
            } else {
                bunt::eprint!("{$dimmed}-{/$} Removing {$green}{}{/$}...\r", file_path).await;
                stderr().flush().await?;
                remove_file(file_path).await?;
                bunt::eprintln!("{$dimmed}-{/$} Removed {$green}{}{/$}    ", file_path).await;
            }
        }

        local_state
            .installed_packages
            .insert(package_name, updating_package);
        local_state.write().await?;
    }

    eprintln!("Finished update.").await;
    Ok(())
}
