use crate::{
    arguments::Install,
    local_state::{InstalledPackage, LocalState},
    registry::{download_file, fetch_package},
    utilities::{check_for_conflicts, confirm, ShowFiles},
    Result,
};
use librust::{
    eprintln,
    fs::{create_dir_all, File, Permissions},
    io::{stderr, Write},
    path::Path,
};
use reqwest::Client;
use semver::Version;
use std::collections::HashMap;

pub async fn install(client: &Client, options: Install) -> Result<()> {
    let mut local_state = LocalState::open().await?;

    let mut packages = HashMap::new();

    let mut met_unknown_packages = false;
    for package_name in options.packages {
        let package = if let Some(package) = fetch_package(client, &package_name).await? {
            package
        } else {
            bunt::eprintln!(
                "{$red}Error:{/$} found no package named {$green}{}{/$}",
                package_name,
            )
            .await;
            met_unknown_packages = true;
            continue;
        };

        let installed_package = match local_state.installed_packages.get(&package_name) {
            Some(package) => package,
            None => {
                packages.insert(package_name, package);
                continue;
            }
        };

        let remote_version = Version::parse(&package.version)?;
        let installed_version = Version::parse(&installed_package.version)?;

        if remote_version > installed_version {
            bunt::eprintln!(
                "Package {$green}{name}{/$} is already installed, but can be \
                 updated. Run {$bold}wpm update {name}{/$} to update the \
                 package.",
                name = package_name,
            )
            .await;
        } else {
            bunt::eprintln!(
                "Package {$green}{}{/$} is already installed and up to date.",
                package_name,
            )
            .await;
        }
    }

    if met_unknown_packages {
        librust::process::exit(1);
    }

    if packages.is_empty() {
        eprintln!("Nothing to install.").await;
        return Ok(());
    }

    if check_for_conflicts(&local_state, &packages).await? {
        eprintln!("Stopping installation due to conflicts.").await;
        librust::process::exit(1);
    }

    eprintln!("Going to install:").await;
    for (package_name, package) in &packages {
        bunt::println!(
            "{$dimmed}-{/$} {$bold}{}{/$} {$dimmed}v{}{/$}",
            package_name,
            package.version,
        )
        .await;
        ShowFiles::stderr()
            .padding(2)
            .show(package.file_paths())
            .await?;
    }

    if !confirm("Proceed to install?", options.assume).await? {
        eprintln!("Stopping installation.").await;
        return Ok(());
    }

    for (package_name, package) in packages {
        bunt::eprintln!("Installing {$green+bold}{}{/$}...", package_name).await;

        let mut installed_package = InstalledPackage {
            version: package.version,
            files: HashMap::new(),
        };

        for (file_path, info) in package.files {
            bunt::eprint!("{$dimmed}-{/$} Downloading {$green}{}{/$}...\r", file_path).await;
            stderr().flush().await?;
            let file_contents = download_file(client, &package_name, &info.fingerprint).await?;

            bunt::eprint!("{$dimmed}-{/$} Installing {$green}{}{/$}... \r", file_path).await;
            stderr().flush().await?;

            create_dir_all(Path::new(&file_path).parent().unwrap()).await?;
            let mut file = File::create(&file_path).await?;
            file.write_all(&file_contents).await?;
            file.set_permissions(Permissions::from_mode(info.permissions))
                .await?;
            // todo: set uid and gid

            bunt::eprintln!("{$dimmed}-{/$} Installed {$green}{}{/$}     ", file_path).await;
            installed_package.files.insert(file_path, info.fingerprint);
        }

        local_state
            .installed_packages
            .insert(package_name, installed_package);
        local_state.write().await?;
    }

    eprintln!("Finished installation.").await;
    Ok(())
}
