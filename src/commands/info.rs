use crate::{arguments::Info, local_state::LocalState, utilities::ShowFiles, Result};
use librust::{
    io::{stderr, Write},
    println,
};
use reqwest::Client;
use std::collections::HashSet;

pub async fn info(client: &Client, options: Info) -> Result<()> {
    if options.packages.is_empty() {
        bunt::eprintln!("{$red}Error:{/$} no packages were specified").await;
        librust::process::exit(1);
    }

    let local_state = LocalState::open().await?;
    let mut has_errored = false;
    for (nth, package) in options.packages.iter().enumerate() {
        if nth > 0 {
            println!().await;
        }

        has_errored |= !package_info(client, package, &local_state).await?;
    }

    if has_errored {
        stderr().flush().await?;
        librust::process::exit(1);
    }

    Ok(())
}

async fn package_info(
    client: &Client,
    package_name: &str,
    local_state: &LocalState,
) -> Result<bool> {
    let package = match crate::registry::fetch_package(client, &package_name).await? {
        Some(package) => package,
        None => {
            bunt::eprintln!(
                "{$red}Error:{/$} found no package named {$green}{}{/$}",
                package_name
            )
            .await;
            return Ok(false);
        }
    };
    let remote_files: HashSet<_> = package.file_paths().collect();
    let local_files = match local_state.installed_packages.get(package_name) {
        Some(installed_package) => installed_package.files.keys().map(String::as_str).collect(),
        None => HashSet::new(),
    };
    let all_files: HashSet<_> = remote_files
        .iter()
        .cloned()
        .chain(local_files.iter().cloned())
        .collect();

    bunt::println!("{$green}{:>11}:{/$} {}", "Name", package_name).await;
    if let Some(description) = &package.description {
        bunt::println!("{$green}{:>11}:{/$} {}", "Description", description).await;
    }

    bunt::print!("{$green}{:>11}:{/$} {}", "Version", package.version).await;
    match local_state.installed_packages.get(package_name) {
        Some(installed_package) => {
            bunt::println!(" {$dimmed}installed v{}{/$}", installed_package.version).await;
        }
        None => println!().await,
    }

    bunt::print!("{$green}{:>11}:{/$} ", "Files").await;
    ShowFiles::stdout()
        .padding(13)
        .first_line_padding(0)
        .show_with(all_files.into_iter(), |file| {
            let is_local = local_files.contains(file);
            let is_remote = remote_files.contains(file);
            async move {
                if !is_local {
                    bunt::print!(" {$dimmed}not installed{/$}").await;
                } else if !is_remote {
                    bunt::print!(" {$red+dimmed}will be removed after update{/$}").await;
                }
            }
        })
        .await?;

    Ok(true)
}
