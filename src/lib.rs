use clap::Clap;
use librust::{eprint, eprintln};
use reqwest::Client;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};
use wasm_bindgen::prelude::*;

type Result<T, E = Box<dyn std::error::Error>> = std::result::Result<T, E>;

mod arguments;
mod commands;
mod local_state;
mod registry;
mod utilities;

use arguments::{Arguments, Command};

#[wasm_bindgen]
pub async fn start() {
    if let Err(error) = main().await {
        let mut stderr = StandardStream::stderr(ColorChoice::Always);
        let _ = stderr
            .set_color(ColorSpec::new().set_fg(Some(Color::Red)))
            .await;
        eprint!("error:").await;
        let _ = stderr.reset().await;
        eprintln!(" {}", error).await;
        librust::process::exit(1);
    }
}

async fn main() -> Result<()> {
    let arguments = Arguments::parse().await;
    let client = Client::new();

    match arguments.command {
        Command::List(options) => commands::list(&client, options).await,
        Command::Info(options) => commands::info(&client, options).await,
        Command::Install(options) => commands::install(&client, options).await,
        Command::Remove(options) => commands::remove(options).await,
        Command::Update(options) => commands::update(&client, options).await,
    }
}
