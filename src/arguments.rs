use std::{
    fmt::{self, Display, Formatter},
    str::FromStr,
};

use clap::Clap;

/// `wpm` stands for Wubix Package Manager. It lets you manage exisiting packages and discover new
/// ones.
#[derive(Debug, Clap)]
#[clap(version = clap::crate_version!())]
pub struct Arguments {
    #[clap(subcommand)]
    pub command: Command,
}

#[derive(Debug, Clap)]
pub enum Command {
    /// Lists available packages and searches through them.
    List(List),
    /// Shows information about packages.
    Info(Info),
    /// Installs packages.
    Install(Install),
    /// Removes packages.
    Remove(Remove),
    /// Updates packages.
    Update(Update),
}

#[derive(Debug, Clap)]
pub struct List {
    /// Search for packages which contain this string in their names.
    pub search: Option<String>,
}

#[derive(Debug, Clap)]
pub struct Info {
    /// Packages to show information about.
    pub packages: Vec<String>,
}

#[derive(Debug, Clap)]
pub struct Install {
    /// Package to install.
    pub packages: Vec<String>,
    /// If set, continues (or stops) installation without prompting.
    #[clap(long)]
    pub assume: Option<Assume>,
}

#[derive(Debug, Clap)]
pub struct Remove {
    /// Package to remove.
    pub packages: Vec<String>,
    /// If set, continues (or stops) removal without prompting.
    #[clap(long)]
    pub assume: Option<Assume>,
}

#[derive(Debug, Clap)]
pub struct Update {
    /// Package to update. If no package is specified, `wpm` will update all installed packages.
    pub packages: Vec<String>,
    /// If set, continues (or stops) updating without prompting.
    #[clap(long)]
    pub assume: Option<Assume>,
}

#[derive(Debug, Clone, Copy)]
pub enum Assume {
    Yes,
    No,
}

#[derive(Debug)]
pub struct AssumeParseError(String);

impl FromStr for Assume {
    type Err = AssumeParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_ascii_lowercase().as_str() {
            "yes" | "y" => Ok(Self::Yes),
            "no" | "n" => Ok(Self::No),
            _ => Err(AssumeParseError(s.to_owned())),
        }
    }
}

impl Display for AssumeParseError {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        write!(formatter, "{}; must be `yes`, `y`, `no` or `n`", self.0)
    }
}
