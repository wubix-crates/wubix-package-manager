use crate::Result;
use librust::{
    eprint,
    io::{stderr, Write},
};
use reqwest::Client;
use serde::Deserialize;
use std::collections::{HashMap, HashSet};

#[derive(Debug, Deserialize)]
pub struct Registry {
    pub packages: HashSet<String>,
}

#[derive(Debug, Deserialize)]
pub struct File {
    pub fingerprint: String,
    pub uid: u32,
    pub gid: u32,
    pub permissions: u32,
}

#[derive(Debug, Deserialize)]
pub struct Package {
    pub description: Option<String>,
    pub version: String,
    pub files: HashMap<String, File>,
}

impl Package {
    pub fn file_paths(&self) -> impl Iterator<Item = &'_ str> + '_ {
        self.files.keys().map(String::as_str)
    }
}

const BASE_URL: &str = "https://wubix-crates.gitlab.io/wubix-package-registry/";

pub async fn fetch_registry(client: &Client) -> Result<Registry> {
    eprint!("Fetching packages...").await;
    let _ = stderr().flush().await;

    let registry_url = format!("{}/registry.json", BASE_URL);
    let registry = client.get(registry_url).send().await?.json().await?;

    bunt::eprintln!(" {$green}done{/$}").await;
    Ok(registry)
}

pub async fn fetch_package(client: &Client, name: &str) -> Result<Option<Package>> {
    let info_url = format!("{}/packages/{}/info.json", BASE_URL, name);

    let response = client.get(info_url).send().await?;
    if response.status() == 404 {
        return Ok(None);
    }

    let info = response.json().await?;
    Ok(Some(info))
}

pub async fn download_file(client: &Client, package: &str, fingerprint: &str) -> Result<Vec<u8>> {
    let file_url = format!("{}/packages/{}/files/{}", BASE_URL, package, fingerprint);
    let response = client.get(file_url).send().await?;
    Ok(response.bytes().await?.to_vec())
}
