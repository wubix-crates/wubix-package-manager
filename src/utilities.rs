use crate::{arguments::Assume, local_state::LocalState, registry::Package, Result};
use librust::{
    eprintln,
    io::{stderr, stdin, Write},
    writeln,
};
use std::{
    collections::{HashMap, HashSet},
    future::{ready, Future},
};
use termcolor::{ColorChoice, StandardStream};
use unix_path::Path;

pub struct ShowFiles {
    stream: StandardStream,
    padding: usize,
    first_line_padding: usize,
}

impl ShowFiles {
    fn new(stream: StandardStream) -> Self {
        Self {
            stream,
            padding: 0,
            first_line_padding: 0,
        }
    }

    pub fn stdout() -> Self {
        Self::new(StandardStream::stdout(ColorChoice::Auto))
    }

    pub fn stderr() -> Self {
        Self::new(StandardStream::stderr(ColorChoice::Auto))
    }

    pub fn padding(mut self, padding: usize) -> Self {
        self.padding = padding;
        self.first_line_padding = padding;
        self
    }

    pub fn first_line_padding(mut self, padding: usize) -> Self {
        self.first_line_padding = padding;
        self
    }

    pub async fn show_with<'a, E, C, F>(mut self, files: E, callback: C) -> Result<()>
    where
        E: Iterator<Item = &'a str>,
        C: for<'b> Fn(&'b str) -> F,
        F: Future<Output = ()> + 'static,
    {
        for (nth, file) in files.enumerate() {
            let path = Path::new(&file);

            let padding = if nth == 0 {
                self.first_line_padding
            } else {
                self.padding
            };

            bunt::write!(
                self.stream,
                "{padding:>width$}{$dimmed}-{/$} {$dimmed}{dir}/{/$}{name}",
                dir = path.parent().unwrap().display(),
                name = path.file_name().unwrap().to_string_lossy(),
                padding = "",
                width = padding,
            )
            .await?;
            callback(&file).await;
            writeln!(&mut self.stream).await?;
        }

        Ok(())
    }

    pub async fn show(self, files: impl Iterator<Item = &'_ str>) -> Result<()> {
        self.show_with(files, |_| ready(())).await
    }
}

pub async fn confirm(message: &str, assume: Option<Assume>) -> Result<bool> {
    match assume {
        Some(Assume::Yes) => return Ok(true),
        Some(Assume::No) => return Ok(false),
        None => (),
    }

    loop {
        bunt::eprint!("{} {$dimmed}(yes/no):{/$} ", message).await;
        stderr().flush().await?;

        let mut answer = String::new();
        stdin().read_line(&mut answer).await?;

        match answer.to_lowercase().as_str().trim() {
            "yes" | "y" => return Ok(true),
            "no" | "n" => return Ok(false),
            _ => eprintln!("Please type yes/y or no/n.").await,
        }
    }
}

pub async fn check_for_conflicts(
    state: &LocalState,
    packages: &HashMap<String, Package>,
) -> Result<bool> {
    let mut files: HashMap<_, _> = state
        .files()
        .map(|(x, y)| (x.to_owned(), y.to_owned()))
        .collect();
    let mut found_conflicts = false;

    // if some files are going to be removed, account for it
    for (package_name, package) in packages {
        let local_package = match state.installed_packages.get(package_name) {
            Some(package) => package,
            None => continue,
        };

        let local_files: HashSet<_> = local_package.files.keys().collect();
        let remote_files: HashSet<_> = package.files.keys().collect();

        let removed_files = local_files.difference(&remote_files);

        for &file in removed_files {
            files.remove(file.as_str());
        }
    }

    for (package_name, package) in packages {
        for file_path in package.files.keys() {
            for (installed_file, provided_by) in &files {
                if provided_by == package_name {
                    continue;
                }

                if installed_file == file_path {
                    found_conflicts = true;
                    bunt::eprintln!(
                        "{$red}Conflict:{/$} package {$green}{installing_package}{/$} provides \
                         {$bold}{installing_file}{/$}, but it is also provided by \
                         {$green}{conflicting_package}{/$}.",
                        installing_package = package_name,
                        installing_file = file_path,
                        conflicting_package = provided_by,
                    )
                    .await;
                }
            }

            files.insert(file_path.clone(), package_name.clone());
        }
    }

    Ok(found_conflicts)
}
