use crate::Result;
use librust::fs::{read, write};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, io::ErrorKind};

const LOCAL_STATE_PATH: &str = "/var/wpm";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InstalledPackage {
    pub version: String,
    pub files: HashMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct LocalState {
    pub installed_packages: HashMap<String, InstalledPackage>,
}

impl LocalState {
    pub async fn open() -> Result<Self> {
        match read(LOCAL_STATE_PATH).await {
            Ok(bytes) => Ok(serde_json::from_slice(&bytes)?),
            Err(error) => {
                if error.kind() == ErrorKind::NotFound {
                    let state = LocalState::default();
                    state.write().await?;
                    Ok(state)
                } else {
                    Err(error.into())
                }
            }
        }
    }

    pub async fn write(&self) -> Result<()> {
        write(LOCAL_STATE_PATH, serde_json::to_vec(&self)?).await?;
        Ok(())
    }

    pub fn files(&self) -> impl Iterator<Item = (&str, &str)> {
        self.installed_packages.iter().flat_map(|(package, info)| {
            let package = package.as_str();
            info.files.keys().map(move |path| (path.as_str(), package))
        })
    }
}
